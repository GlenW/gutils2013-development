using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace GUtils2013.DataAccess
{
	public class MySqlTransactionDataAccess : IDataAccess, IDisposable
	{
		
		#region Properties
		
		public string ConnectionString { get; set; }
		
		public int CommandTimeout { get; set; }
		
		private MySqlCommand Command { get; set; }
		
		private MySqlTransaction Transaction { get; set; }
		
		#endregion Properties
		
		#region IDataAccess
		
		public void ExecuteProcedure(string procedure, params object[] parameters)
		{
			using(this.Command = DataAccessUtils.GetMySqlCommand (procedure, this.Command, parameters))
			{
				if(this.Command.Connection == null)
				{
					this.Command.Connection = new MySqlConnection(this.ConnectionString);
					this.Command.CommandTimeout = this.CommandTimeout;
					this.Command.Connection.Open ();
					this.Transaction = this.Command.Connection.BeginTransaction ();
				}
				this.Command.ExecuteNonQuery ();
			}
		}
		
		public object ExecuteObjectProcedure(string procedure, params object[] parameters)
		{
			using(this.Command = DataAccessUtils.GetMySqlCommand (procedure, this.Command, parameters))
			{
				if(this.Command.Connection == null)
				{
					this.Command.Connection = new MySqlConnection(this.ConnectionString);
					this.Command.CommandTimeout = this.CommandTimeout;
					this.Command.Connection.Open ();
					this.Transaction = this.Command.Connection.BeginTransaction ();
				}
				return this.Command.ExecuteScalar ();
			}
		}
		
		public int ExecuteIntProcedure(string procedure, params object[] parameters)
		{
			using(this.Command = DataAccessUtils.GetMySqlCommand (procedure, this.Command, parameters))
			{
				if(this.Command.Connection == null)
				{
					this.Command.Connection = new MySqlConnection(this.ConnectionString);
					this.Command.CommandTimeout = this.CommandTimeout;
					this.Command.Connection.Open ();
					this.Transaction = this.Command.Connection.BeginTransaction ();
				}
				return Convert.ToInt32 (this.Command.ExecuteScalar ());
			}
		}
		
		public IDataReader OpenDataReader(string procedure, params object[] parameters)
		{
			MySqlDataReader retVal;
			using(this.Command = DataAccessUtils.GetMySqlCommand (procedure, this.Command, parameters))
			{
				if(this.Command.Connection == null)
				{
					this.Command.Connection = new MySqlConnection(this.ConnectionString);
					this.Command.CommandTimeout = this.CommandTimeout;
					this.Command.Connection.Open ();
					this.Transaction = this.Command.Connection.BeginTransaction ();
				}
				retVal = this.Command.ExecuteReader ();
			}
			return retVal;
		}
		
		#endregion IDataAccess
		
		public void Commit()
		{
			this.Transaction.Commit ();
		}
		
		public void RollBack()
		{
			this.Transaction.Rollback ();
		}
		
		#region IDisposable
		
		public void Dispose()
		{
			if(this.Transaction != null)
			{
				this.Transaction.Dispose ();
			}
		}
		
		#endregion IDisposable
		
		#region Constructors
		
		public MySqlTransactionDataAccess (string connectionString)
		{
			this.ConnectionString = connectionString;
		}
		
		#endregion Constructors
		
	}
}

