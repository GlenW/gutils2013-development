using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace GUtils2013.DataAccess
{
	public class MySqlDataAccess : IDataAccess, IDisposable
	{
		
		#region Properties
		
		public string ConnectionString { get; set; }
		
		public int CommandTimeout { get; set; }
		
		#endregion Properties
		
		#region IDataAccess
		
		public void ExecuteProcedure(string procedure, params object[] parameters)
		{
			using(MySqlCommand command = DataAccessUtils.GetMySqlCommand (procedure, null, parameters))
			{
				using(command.Connection = new MySqlConnection(this.ConnectionString))
				{
					command.CommandTimeout = this.CommandTimeout;
					command.Connection.Open ();
					command.ExecuteNonQuery ();
				}
			}
		}
		
		public object ExecuteObjectProcedure(string procedure, params object[] parameters)
		{
			using(MySqlCommand command = DataAccessUtils.GetMySqlCommand (procedure, null, parameters))
			{
				using(command.Connection = new MySqlConnection(this.ConnectionString))
				{
					command.CommandTimeout = this.CommandTimeout;
					command.Connection.Open ();
					return command.ExecuteScalar ();
				}
			}
		}
		
		public int ExecuteIntProcedure(string procedure, params object[] parameters)
		{
			using(MySqlCommand command = DataAccessUtils.GetMySqlCommand (procedure, null, parameters))
			{
				using(command.Connection = new MySqlConnection(this.ConnectionString))
				{
					command.CommandTimeout = this.CommandTimeout;
					command.Connection.Open ();
					return Convert.ToInt32 (command.ExecuteScalar ());
				}
			}
		}
		
		public IDataReader OpenDataReader(string procedure, params object[] parameters)
		{
			MySqlDataReader retVal;
			using(MySqlCommand command = DataAccessUtils.GetMySqlCommand (procedure, null, parameters))
			{
				command.Connection = new MySqlConnection(this.ConnectionString);
				command.CommandTimeout = this.CommandTimeout;
				command.Connection.Open ();
				retVal = command.ExecuteReader ();
			}
			return retVal;
		}
		
		#endregion IDataAccess
		
		#region IDisposable
		
		public void Dispose()
		{
		}
		
		#endregion IDisposable
		
		#region Constructors
		
		public MySqlDataAccess (string connectionString)
		{
			this.ConnectionString = connectionString;
		}
		
		#endregion Constructors
		
	}
}

