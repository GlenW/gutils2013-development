using System;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace GUtils2013.DataAccess
{
	internal static class DataAccessUtils
	{
		
		#region SqlServer Methods
		
		private static int MAX_SQLSERVER_VARCHAR_LENGTH = 8000;
		
		internal static SqlCommand GetSqlCommand(string procedure, SqlCommand command, params object[] parameters)
		{
			if(command == null)
			{
				command = new SqlCommand (procedure);
			}
			SqlParameter parameter;
			if(parameters != null)
			{
				command.Parameters.Clear ();
				for (int i = parameters.GetLowerBound(0); i < parameters.GetUpperBound(0); i = i + 2) 
				{
					object val = parameters [i + 1];
					if (val is bool) 
					{
						if ((bool)val == true) 
						{
							val = 1;
						}
						else 
						{
							val = 0;
						}
					}
					parameter = new SqlParameter (parameters [i].ToString (), getMySqlDbType (val));
					parameter.IsNullable = true;
					parameter.Value = val;
					command.Parameters.Add (parameter);
				}
			}
			command.CommandType = System.Data.CommandType.StoredProcedure;
			return command;
		}
		
		internal static SqlDbType getMySqlDbType(object obj)
		{
			if (obj is Int32) 
			{
				return SqlDbType.Int;
			}
			else if (obj is Int16) 
			{
				return SqlDbType.SmallInt;
			}
			else if (obj is Int64) 
			{
				return SqlDbType.BigInt;
			}
			else if (obj is byte) 
			{
				return SqlDbType.TinyInt;
			}
			else if (obj is string) 
			{
				if(obj.ToString ().Length > MAX_SQLSERVER_VARCHAR_LENGTH)
				{
					return SqlDbType.Text;
				}
				else
				{
					return SqlDbType.VarChar;
				}
			}
			else if (obj is bool) 
			{
				return SqlDbType.Bit;
			}
			else if (obj is DateTime) 
			{
				return SqlDbType.DateTime;
			}
			else if (obj is byte[]) 
			{
				return SqlDbType.Image;
			}
			else if (obj is char) 
			{
				return SqlDbType.Char;
			}
			else if (obj is decimal) 
			{
				return SqlDbType.Decimal;
			}
			else if (obj is float) 
			{
				return SqlDbType.Float;
			}
			else 
			{
				return SqlDbType.Variant;
			}
		}
		
		#endregion Sqlserver Methods
		
		#region MySql Methods
		
		internal static MySqlCommand GetMySqlCommand(string procedure, MySqlCommand command, params object[] parameters)
		{
			if(command == null)
			{
				command = new MySqlCommand (procedure);
			}
			MySqlParameter parameter;
			if(parameters != null)
			{
				command.Parameters.Clear ();
				for (int i = parameters.GetLowerBound(0); i < parameters.GetUpperBound(0); i = i + 2) 
				{
					object val = parameters [i + 1];
					if (val is bool) 
					{
						if ((bool)val == true) 
						{
							val = 1;
						}
						else 
						{
							val = 0;
						}
					}
					parameter = new MySqlParameter (parameters [i].ToString (), getMySqlDbType (val));
					parameter.IsNullable = true;
					parameter.Value = val;
					command.Parameters.Add (parameter);
				}
			}
			command.CommandType = System.Data.CommandType.StoredProcedure;
			return command;
		}
		
		internal static MySqlDbType getSqlDbType(object obj)
		{
			if (obj is Int32) 
			{
				return MySqlDbType.Int32;
			}
			else if (obj is Int16) 
			{
				return MySqlDbType.Int16;
			}
			else if (obj is Int64) 
			{
				return MySqlDbType.Int64;
			}
			else if (obj is string) 
			{
				return MySqlDbType.VarChar;
			}
			else if (obj is bool) 
			{
				return MySqlDbType.Bit;
			}
			else if (obj is DateTime) 
			{
				return MySqlDbType.DateTime;
			}
			else if (obj is byte[]) 
			{
				return MySqlDbType.Blob;
			}
			else if (obj is char) 
			{
				return MySqlDbType.VarChar;
			}
			else if (obj is decimal) 
			{
				return MySqlDbType.Decimal;
			}
			else 
			{
				return MySqlDbType.VarChar;
			}
		}
		
		#endregion MySql Methods
		
	}
}

