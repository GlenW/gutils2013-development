using System;
using System.Data;

namespace GUtils2013.DataAccess
{
	/// <summary>
	/// IDataAccess.
	/// </summary>
	public interface IDataAccess
	{
		
		/// <summary>
		/// Executes procedure with parameters NonQuery
		/// </summary>
		/// <param name='procedure'>
		/// Procedure. The stored procedure to execute
		/// </param>
		/// <param name='parameters'>
		/// Parameters. Use pairs as "@ParamName1", value1, "@ParamName2", value2. Null for no paramters
		/// </param>
		void ExecuteProcedure(string procedure, params object[] parameters);
		
		/// <summary>
		/// Executes procedure with parameters Scalar and returns object value
		/// </summary>
		/// <returns>
		/// object
		/// </returns>
		/// <param name='procedure'>
		/// Procedure. The stored procedure to execute
		/// </param>
		/// <param name='parameters'>
		/// Parameters. Use pairs as "@ParamName1", value1, "@ParamName2", value2. Null for no paramters
		/// </param>
		object ExecuteObjectProcedure(string procedure, params object[] parameters);
		
		/// <summary>
		/// Executes procedure with parameters Scalar and returns ConvertToInt32(value)
		/// </summary>
		/// <returns>
		/// int
		/// </returns>
		/// <param name='procedure'>
		/// Procedure. The stored procedure to execute
		/// </param>
		/// <param name='parameters'>
		/// Parameters. Use pairs as "@ParamName1", value1, "@ParamName2", value2. Null for no paramters
		/// </param>
		int ExecuteIntProcedure(string procedure, params object[] parameters);
		
		/// <summary>
		/// Returns an IDataReader from procedure with parameters
		/// </summary>
		/// <returns>
		/// IDataReader
		/// </returns>
		/// <param name='procedure'>
		/// Procedure. The stored procedure to execute
		/// </param>
		/// <param name='parameters'>
		/// Parameters. Use pairs as "@ParamName1", value1, "@ParamName2", value2. Null for no paramters
		/// </param>
		IDataReader OpenDataReader(string procedure, params object[] parameters);
		
	}
}

