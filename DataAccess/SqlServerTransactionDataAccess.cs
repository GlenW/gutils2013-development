using System;
using System.Data;
using System.Data.SqlClient;

namespace GUtils2013.DataAccess
{
	public class SqlServerTransactionDataAccess : IDataAccess, IDisposable
	{
		
		#region Properties
		
		public int CommandTimeout { get; set; }
		
		private string ConnectionString { get; set; }
		
		private SqlCommand Command { get; set; }
		
		private SqlTransaction Transaction { get; set; }
		
		#endregion Properties
		
		#region IDataAccess
		
		public void ExecuteProcedure(string procedure, params object[] parameters)
		{
			using(this.Command = DataAccessUtils.GetSqlCommand (procedure, this.Command, parameters))
			{
				if(this.Command.Connection == null)
				{
					this.Command.Connection = new SqlConnection(this.ConnectionString);
					this.Command.CommandTimeout = this.CommandTimeout;
					this.Command.Connection.Open ();
					this.Transaction = this.Command.Connection.BeginTransaction ();
				}
				this.Command.ExecuteNonQuery ();
			}
		}
		
		public object ExecuteObjectProcedure(string procedure, params object[] parameters)
		{
			using(this.Command = DataAccessUtils.GetSqlCommand (procedure, this.Command, parameters))
			{
				if(this.Command.Connection == null)
				{
					this.Command.Connection = new SqlConnection(this.ConnectionString);
					this.Command.CommandTimeout = this.CommandTimeout;
					this.Command.Connection.Open ();
					this.Transaction = this.Command.Connection.BeginTransaction ();
				}
				return this.Command.ExecuteScalar ();
			}
		}
		
		public int ExecuteIntProcedure(string procedure, params object[] parameters)
		{
			using(this.Command = DataAccessUtils.GetSqlCommand (procedure, this.Command, parameters))
			{
				if(this.Command.Connection == null)
				{
					this.Command.Connection = new SqlConnection(this.ConnectionString);
					this.Command.CommandTimeout = this.CommandTimeout;
					this.Command.Connection.Open ();
					this.Transaction = this.Command.Connection.BeginTransaction ();
				}
				return Convert.ToInt32 (this.Command.ExecuteScalar ());
			}
		}
		
		public IDataReader OpenDataReader(string procedure, params object[] parameters)
		{
			SqlDataReader retVal;
			using(this.Command = DataAccessUtils.GetSqlCommand (procedure, this.Command, parameters))
			{
				if(this.Command.Connection == null)
				{
					this.Command.Connection = new SqlConnection(this.ConnectionString);
					this.Command.CommandTimeout = this.CommandTimeout;
					this.Command.Connection.Open ();
					this.Transaction = this.Command.Connection.BeginTransaction ();
				}
				retVal = this.Command.ExecuteReader ();
			}
			return retVal;
		}
		
		#endregion IDataAccess
		
		public void Commit()
		{
			this.Transaction.Commit ();
		}
		
		public void RollBack()
		{
			if(this.Transaction != null)
			{
				this.Transaction.Rollback ();
			}
		}
		
		#region IDisposable
		
		public void Dispose()
		{
			this.Transaction.Dispose ();
		}
		
		#endregion IDisposable
		
		#region Constructors
		
		public SqlServerTransactionDataAccess (string connectionString)
		{
			this.ConnectionString = connectionString;
		}
		
		#endregion Constructors
		
	}
}

