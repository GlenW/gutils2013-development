﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GUtils2013.ChangeMonitor
{

  /// <summary>
  /// Extension methods to call ChangeMonitorSelfPopulater methods via IChangeMonitorSelfPopulater
  /// </summary>
  public static class IChangeMonitorExtensions
  {

    /// <summary>
    /// Populates copies of obj's properties and sets Enabled = true
    /// </summary>
    /// <param name="obj">IChangeMonitor obj</param>
    public static void SetAsEnabled(this IChangeMonitor obj)
    {
      ChangeMonitor.SetAsEnabled(obj);
    }

    /// <summary>
    /// Sets obj.Enabled = false, GetChanged will always return False until Enabled is set back to True
    /// </summary>
    /// <param name="obj"></param>
    public static void SetAsDisabled(this IChangeMonitor obj)
    {
      ChangeMonitor.SetAsDisabled(obj);
    }

    /// <summary>
    /// <para>Returns if obj's properties have changed since obj.SetAsEnabled or obj.SetAsNotChanged called.</para>
    /// <para>Always returns False if obj.Enabled == false</para>
    /// </summary>
    /// <param name="obj">IChangeMonitor obj</param>
    /// <returns>bool</returns>
    public static bool GetChanged(this IChangeMonitor obj)
    {
      return ChangeMonitor.GetChanged(obj);
    }

    /// <summary>
    /// Refreshes property copies
    /// </summary>
    /// <param name="obj"></param>
    public static void SetAsNotChanged (this IChangeMonitor obj)
    {
      ChangeMonitor.SetAsNotChanged(obj);
    }

    /// <summary>
    /// Resets property values back to the original values when set as enabled
    /// </summary>
    /// <param name="obj"></param>
    public static void ResetProperties(this IChangeMonitor obj)
    {
      ChangeMonitor.ResetProperties(obj);
    }

  }
}
