﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GUtils2013.ChangeMonitor
{
  /*
   ################################################################
   # WARNING ONLY USE THESE METHODS TO IMPLEMENT IChangeMonitor   #
   # NEVER ACCESS Enabled or PropertyCopies DIRECTLY              #
   # public static void SetAsEnabled(this IChangeMonitor obj)     #
   # public static void SetAsDisabled(this IChangeMonitor obj)    #
   # public static bool GetChanged(this IChangeMonitor obj)       #
   # public static void SetAsNotChanged (this IChangeMonitor obj) #
   * ##############################################################
   */
  /// <summary>
  /// <para>WARNING ONLY USE THESE METHODS TO IMPLEMENT IChangeMonitor</para>
  /// <para>NEVER ACCESS Enabled or PropertyCopies DIRECTLY</para>
  /// <para>public static void SetAsEnabled(this IChangeMonitor obj)</para>
  /// <para>public static void SetAsDisabled(this IChangeMonitor obj)</para>
  /// <para>public static bool GetChanged(this IChangeMonitor obj)</para>
  /// <para>public static void SetAsNotChanged (this IChangeMonitor obj)</para>
  /// </summary>
  public interface IChangeMonitor
  {

    /// <summary>
    /// <para>bool IChanged.Enabled</para>
    /// <para>WARNING: Never access diredtly</para>
    /// </summary>
    Boolean Enabled { get; set; }

    /// <summary>
    /// <para>bool IChanged.PropertyCopies</para>
    /// <para>WARNING: Never access directly</para>
    /// </summary>
    List<object> PropertyCopies { get; set; }

  }
}
