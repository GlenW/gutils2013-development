﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;

namespace GUtils2013.ChangeMonitor
{
  internal static class ChangeMonitor
  {

    #region Internal Methods

    internal static void SetAsEnabled(IChangeMonitor obj)
    {
      populatePropertyCopies(obj);
      obj.Enabled = true;
    }

    internal static void SetAsDisabled(IChangeMonitor obj)
    {
      obj.Enabled = false;
    }

    internal static bool GetChanged(IChangeMonitor obj)
    {
      if (obj.Enabled)
      {
        if (obj.PropertyCopies == null)
        {
          populatePropertyCopies(obj);
        }
        return getIfChanged(obj);
      }
      else
      {
        return false;
      }
    }

    internal static void SetAsNotChanged(IChangeMonitor obj)
    {
      obj.Enabled = true;
      populatePropertyCopies(obj);
    }

    internal static void ResetProperties(IChangeMonitor obj)
    {
      if (obj.Enabled)
      {
        int position = -1;
        foreach (PropertyInfo info in obj.GetType().GetProperties())
        {
          if (info.GetCustomAttributes(typeof(ChangeProperty), true).Length == 1)
          {
            position++;
            if (null == ((IChangeMonitor)obj).PropertyCopies[position] || null == info.GetValue(obj, null))
            {
              if (!(null == ((IChangeMonitor)obj).PropertyCopies[position] && null == info.GetValue(obj, null)))
              {
                info.SetValue(obj, ((IChangeMonitor)obj).PropertyCopies[position], null);
              }
            }
            else
            {
              if (info.GetValue(obj, null).ToString() != ((IChangeMonitor)obj).PropertyCopies[position].ToString())
              {
                info.SetValue(obj, ((IChangeMonitor)obj).PropertyCopies[position], null);
              }
            }
          }
        }
      }
    }

    #endregion Internal Methods

    #region Private Methods

    private static void populatePropertyCopies(IChangeMonitor obj)
    {
      if (obj.PropertyCopies == null)
      {
        obj.PropertyCopies = new List<object>();
      }
      else
      {
        obj.PropertyCopies.Clear();
      }
      foreach (PropertyInfo info in obj.GetType().GetProperties())
      {
        if (info.GetCustomAttributes(typeof(ChangeProperty), true).Length == 1)
        {
          obj.PropertyCopies.Add(info.GetValue(obj, null));
        }
      }
    }

    private static bool getIfChanged(IChangeMonitor obj)
    {
      if (obj.Enabled)
      {
        int position = -1;
        foreach (PropertyInfo info in obj.GetType().GetProperties())
        {
          if(info.GetCustomAttributes(typeof(ChangeProperty), true).Length == 1)
          {
            position++;
            if (null == ((IChangeMonitor)obj).PropertyCopies[position] || null == info.GetValue(obj, null))
            {
              if (!(null == ((IChangeMonitor)obj).PropertyCopies[position] && null == info.GetValue(obj, null)))
              {
                return true;
              }
            }
            else
            {
              if (info.GetValue(obj, null).ToString() != ((IChangeMonitor)obj).PropertyCopies[position].ToString())
              {
                return true;
              }
            }
          }
        }
      }
      return false;
    }

    #endregion Private Methods

  }
}
