﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace GUtils2013.GWindows
{

  /// <summary>
  /// NullableDTP
  /// </summary>
  public partial class NullableDTP : DateTimePicker, ISupportInitialize
  {

    #region Member Variables Added To Allow Null Values

    private DateTimePickerFormat _Format;
    private string _CustomFormat;
    private string _nullText = "";

    #endregion

    #region Member Variables Added To Enable ReadOnly Mode

    private bool _readOnly = false;
    private bool _visible = true;
    private bool _tabStopWhenReadOnly = false;
    private TextBox _textBox;

    #endregion

    #region Constructor

    /// <summary>
    /// 
    /// </summary>
    public NullableDTP()
    {
      InitializeComponent();
      initTextBox();
      base.Format = DateTimePickerFormat.Custom;
      _Format = DateTimePickerFormat.Long;
      if (DesignMode)
        setFormat();
    }

    #endregion

    #region ISupportInitialize Members

    private bool initializing = true;

    /// <summary>
    /// 
    /// </summary>
    public void BeginInit()
    {
      initializing = true;
    }

    /// <summary>
    /// 
    /// </summary>
    public void EndInit()
    {
      base.Value = DateTime.Today;
      initializing = false;
      if (DesignMode)
      {
        return;
      }
      if (this.Parent.GetType() == typeof(TableLayoutPanel))
      {
        TableLayoutPanelCellPosition cP = ((TableLayoutPanel)this.Parent).GetPositionFromControl(this);
        ((TableLayoutPanel)this.Parent).Controls.Add(_textBox, cP.Column, cP.Row);
        ((TableLayoutPanel)this.Parent).SetColumnSpan(_textBox, ((TableLayoutPanel)this.Parent).GetColumnSpan(this));
        _textBox.Anchor = this.Anchor;
      }
      else if (this.Parent.GetType() == typeof(FlowLayoutPanel))
      {
        ((FlowLayoutPanel)this.Parent).Controls.Add(_textBox);
        ((FlowLayoutPanel)this.Parent).Controls.SetChildIndex(_textBox, ((FlowLayoutPanel)this.Parent).Controls.IndexOf(this));
        _textBox.Anchor = this.Anchor;
      }
      else
      {
        _textBox.Parent = this.Parent;
        _textBox.Anchor = this.Anchor;
      }

      Control parent = this;
      bool foundLoadingParent = false;
      do
      {
        parent = parent.Parent;
        if (parent.GetType().IsSubclassOf(typeof(UserControl)))
        {
          ((UserControl)parent).Load += new EventHandler(UltraDateTimePicker_Load);
          foundLoadingParent = true;
        }
        else if (parent.GetType().IsSubclassOf(typeof(Form)))
        {
          ((Form)parent).Load += new EventHandler(UltraDateTimePicker_Load);
          foundLoadingParent = true;
        }
      }
      while (!foundLoadingParent);
    }
    void UltraDateTimePicker_Load(object sender, EventArgs e)
    {
      setVisibility();
    }

    #endregion

    #region Public Properties Modified/Added To Allow Null Values

    private bool _isNull = false;

    /// <summary>
    /// 
    /// </summary>
    new public Object Value
    {
      get
      {
        if (_isNull)
        {
          return null;
        }
        else
        {
          return base.Value;
        }
      }

      set
      {
        if (value == null || value == DBNull.Value)
        {
          if (!_isNull)
          {
            _isNull = true;
            this.OnValueChanged(EventArgs.Empty);
          }
        }
        else
        {
          if (_isNull && base.Value == (DateTime)value)
          {
            _isNull = false;
            this.OnValueChanged(EventArgs.Empty);
          }
          else
          {
            _isNull = false;
            base.Value = (DateTime)value;
          }
        }
        setFormat();
        _textBox.Text = this.Text;
      }
    }

    #region DesignerModifiers
    /// <summary>
    /// 
    /// </summary>
    [Browsable(true)]
    [Category("Behavior")]
    [Description("Text shown when DateTime is 'null'")]
    [DefaultValue("")]

    #endregion

    public string NullText
    {
      get { return _nullText; }
      set { _nullText = value; }
    }

    #region DesignerModifiers
    /// <summary>
    /// 
    /// </summary>
    [Browsable(true)]
    [DefaultValue(DateTimePickerFormat.Long), TypeConverter(typeof(Enum))]

    #endregion

    new public DateTimePickerFormat Format
    {
      get { return this._Format; }
      set
      {
        this._Format = value;
        this.setFormat();
      }
    }

    private void setFormat()
    {
      base.CustomFormat = null;
      if (_isNull)
      {
        base.CustomFormat = String.Concat("'", this.NullText, "'");
      }
      else
      {
        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        DateTimeFormatInfo dTFormatInfo = cultureInfo.DateTimeFormat;
        switch (_Format)
        {
          case DateTimePickerFormat.Long:
            base.CustomFormat = dTFormatInfo.LongDatePattern;
            break;
          case DateTimePickerFormat.Short:
            base.CustomFormat = dTFormatInfo.ShortDatePattern;
            break;
          case DateTimePickerFormat.Time:
            base.CustomFormat = dTFormatInfo.ShortTimePattern;
            break;
          case DateTimePickerFormat.Custom:
            base.CustomFormat = this._CustomFormat;
            break;
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    new public string CustomFormat
    {
      get { return _CustomFormat; }
      set
      {
        this._CustomFormat = value;
        this.setFormat();
      }
    }

    #endregion

    #region Public Properties Modified/Added To Enable ReadOnly Mode

    #region DesignerModifiers
    /// <summary>
    /// 
    /// </summary>
    [Browsable(true)]
    [Category("Behavior")]
    [Description("Displays Control as ReadOnly(Black on Gray) if 'true'")]
    [DefaultValue(false)]

    #endregion

    public bool ReadOnly
    {
      get { return _readOnly; }
      set
      {
        this._readOnly = value;
        setVisibility();
      }
    }

    #region DesignerModifiers
    /// <summary>
    /// 
    /// </summary>
    [Category("Behavior")]
    [DefaultValue(false)]
    [Browsable(true)]
    [EditorBrowsable(EditorBrowsableState.Always)]

    #endregion

    public bool TabstopWhenReadOnly
    {
      get { return _tabStopWhenReadOnly; }
      set
      {
        _tabStopWhenReadOnly = value;
        _textBox.TabStop = (_tabStopWhenReadOnly && this.TabStop); //TextBox is a Tabstop only if mimicing and DTP is a TabStop
      }
    }

    /// <summary>
    /// 
    /// </summary>
    new public bool TabStop
    {
      get { return base.TabStop; }
      set
      {
        base.TabStop = value;
        _textBox.TabStop = (_tabStopWhenReadOnly && base.TabStop);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    new public bool Visible
    {
      get { return _visible; }
      set
      {
        _visible = value;
        setVisibility();
      }
    }

    #endregion

    #region OnXXXX() Modified To Allow Null Values

    /// <summary>
    /// 
    /// </summary>
    /// <param name="m"></param>
    protected override void WndProc(ref Message m)
    {
      if (m.Msg == 0x4e)
      {
        NMHDR nm = (NMHDR)m.GetLParam(typeof(NMHDR));
        if (nm.Code == -746 || nm.Code == -722)
        {
          this.Value = base.Value;
        }
      }
      base.WndProc(ref m);
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct NMHDR
    {
      public IntPtr HwndFrom;
      public int IdFrom;
      public int Code;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnKeyUp(KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
      {
        this.Value = null;
      }
      base.OnKeyUp(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      base.OnKeyPress(e);
      if (_isNull && Char.IsDigit(e.KeyChar))
      {
        this.Value = base.Value;
        e.Handled = true;
        SendKeys.Send("{RIGHT}");
        SendKeys.Send(e.KeyChar.ToString());
      }
      else
      {
        base.OnKeyPress(e);
      }
    }

    #endregion

    #region OnXXXX() Modified To Enable ReadOnly Mode

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnParentChanged(EventArgs e)
    {
      base.OnParentChanged(e);
      if (DesignMode || initializing)
      {
        return;
      }
      updateReadOnlyTextBoxParent();
      setVisibility();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLocationChanged(EventArgs e)
    {
      base.OnLocationChanged(e);
      _textBox.Location = this.Location;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);
      _textBox.Size = this.Size;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      _textBox.Size = this.Size;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnDockChanged(EventArgs e)
    {
      base.OnDockChanged(e);
      _textBox.Dock = this.Dock;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnRightToLeftChanged(EventArgs e)
    {
      base.OnRightToLeftChanged(e);
      _textBox.RightToLeft = this.RightToLeft;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnTabStopChanged(EventArgs e)
    {
      base.OnTabStopChanged(e);
      _textBox.TabStop = _tabStopWhenReadOnly && this.TabStop;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnTabIndexChanged(EventArgs e)
    {
      base.OnTabIndexChanged(e);
      _textBox.TabIndex = this.TabIndex;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnFontChanged(EventArgs e)
    {
      base.OnFontChanged(e);
      _textBox.Font = this.Font;
    }

    #endregion

    #region Private Methods Added To Enable ReadOnly Mode

    private void initTextBox()
    {
      if (DesignMode)
      {
        return;
      }
      _textBox = new TextBox();
      _textBox.ReadOnly = true;
      _textBox.Location = this.Location;
      _textBox.Size = this.Size;
      _textBox.Dock = this.Dock;
      _textBox.Anchor = this.Anchor;
      _textBox.RightToLeft = this.RightToLeft;
      _textBox.Font = this.Font;
      _textBox.TabStop = this.TabStop;
      _textBox.TabIndex = this.TabIndex;
      _textBox.Visible = false;
      _textBox.Parent = this.Parent;
    }

    private void setVisibility()
    {
      if (DesignMode || initializing)
      {
        return;
      }
      if (this._visible)
      {
        if (this._readOnly)
        {
          showTextBox();
        }
        else
        {
          showDTP();
        }
      }
      else
      {
        showNone();
      }
    }

    private void showTextBox()
    {
      base.Visible = false;
      _textBox.Visible = true;
      _textBox.TabStop = _tabStopWhenReadOnly && this.TabStop;
    }

    private void showDTP()
    {
      _textBox.Visible = false;
      base.Visible = true;
    }

    private void showNone()
    {
      _textBox.Visible = false;
      base.Visible = false;
    }

    private void updateReadOnlyTextBoxParent()
    {
      if (this.Parent == null) //If UTDP.Parent == null, set _textBox.Parent == null and return
      {
        _textBox.Parent = null;
        return;
      }
      if (_textBox.Parent != this.Parent) //If the Parents DO NOT already match
      {
        if (this.Parent.GetType() == typeof(TableLayoutPanel))
        {
          TableLayoutPanelCellPosition cP = ((TableLayoutPanel)this.Parent).GetPositionFromControl(this);
          ((TableLayoutPanel)this.Parent).Controls.Add(_textBox, cP.Column, cP.Row);
          ((TableLayoutPanel)this.Parent).SetColumnSpan(_textBox, ((TableLayoutPanel)this.Parent).GetColumnSpan(this));
          _textBox.Anchor = this.Anchor;
        }
        else if (this.Parent.GetType() == typeof(FlowLayoutPanel))
        {
          ((FlowLayoutPanel)this.Parent).Controls.Add(_textBox);
          ((FlowLayoutPanel)this.Parent).Controls.SetChildIndex(_textBox, ((FlowLayoutPanel)this.Parent).Controls.IndexOf(this));
          _textBox.Anchor = this.Anchor;
        }
        else
        {
          _textBox.Parent = this.Parent;
          _textBox.Anchor = this.Anchor;
        }
      }
    }

    #endregion

    #region Public Methods Overriden To Enable ReadOnly Mode

    /// <summary>
    /// 
    /// </summary>
    new public void Show()
    {
      this.Visible = true;
    }

    /// <summary>
    /// 
    /// </summary>
    new public void Hide()
    {
      this.Visible = false;
    }

    #endregion

  }

}

