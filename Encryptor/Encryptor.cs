﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace GUtils2013.Encryptor
{

	/// <summary>
	/// Contains methods to enable simple synchronous encryption and decryptrion
	/// </summary>
	public sealed class Encryptor
	{

    #region Constants

		// Default cryptoKey to use if none specified in method calls.
		private static string CRYPTO_KEY
		{
			get
			{
				return ConfigurationManager.AppSettings[CRYPTO_KEY] ?? "1bc2ed2b-56ee-4e83-b5aa-bf212a8c9f9c";
			}
		}
	
		// Default initialization vector for the DES encryption routine if none specified in method calls.
		private static readonly byte[] INIT_VECTOR = new byte[8] { 238, 7, 31, 42, 13, 86, 191, 21 };

    #endregion Constants

    #region Static Methods

    #region Public Static Methods

		/// <summary>
		/// Encrypts stringToEncrypt, uses default CryptoKey
		/// </summary>
		/// <param name="stringToEncrypt">String to encrypt</param>
		/// <returns>Encrypted string</returns>
		public static string Encrypt(string stringToEncrypt)
		{
			if (String.IsNullOrEmpty (stringToEncrypt)) {
				return string.Empty;
			}
			return doEncrypt (stringToEncrypt, CRYPTO_KEY, INIT_VECTOR);
		}

		/// <summary>
		/// Decrypts stringToDecrypt, uses default CryptoKey
		/// </summary>
		/// <param name="stringToDecrypt">String to decrypt, must have been encrypted using EncryptionHelper.Encrypt</param>
		/// <returns></returns>
		public static string Decrypt(string stringToDecrypt)
		{
			if (String.IsNullOrEmpty (stringToDecrypt)) {
				return string.Empty;
			}
			return doDecrypt (stringToDecrypt, CRYPTO_KEY, INIT_VECTOR);
		}

		/// <summary>
		/// Encrypts stringToEncrypt
		/// </summary>
		/// <param name="stringToEncrypt">String to encrypt</param>
		/// <param name="cryptoKey">String to use as CryptoKey</param>
		/// <returns></returns>
		public static string Encrypt(string stringToEncrypt, string cryptoKey)
		{
			if (String.IsNullOrEmpty (stringToEncrypt)) {
				return string.Empty;
			}
			return doEncrypt (stringToEncrypt, cryptoKey, INIT_VECTOR);
		}

		/// <summary>
		/// Decrypts stringToDecrypt
		/// </summary>
		/// <param name="stringToDecrypt">String to decrypt, must have been encrypted using EncryptionHelper.Encrypt with same cryptoKey.</param>
		/// <param name="cryptoKey">String to use as CryptoKey.</param>
		/// <returns>String to use as CryptoKey</returns>
		public static string Decrypt(string stringToDecrypt, string cryptoKey)
		{
			if (String.IsNullOrEmpty (stringToDecrypt)) {
				return string.Empty;
			}
			return doEncrypt (stringToDecrypt, cryptoKey, INIT_VECTOR);
		}

		/// <summary>
		/// Encrypts stringToEncrypt
		/// </summary>
		/// <param name="stringToEncrypt">String to encrypt</param>
		/// <param name="cryptoKey">String to use as CryptoKey</param>
		/// <param name="initVector">byte[8] to use as initialization vector</param>
		/// <returns></returns>
		/// <exception cref="IndexOutOfRangeException">IndexOutOfRangeException thrown if initVector is not 8 elements.</exception>
		public static string Encrypt(string stringToEncrypt, string cryptoKey, byte[] initVector)
		{
			return doEncrypt (stringToEncrypt, cryptoKey, initVector);
		}

		/// <summary>
		/// Decrypts stringToDecrypt
		/// </summary>
		/// <param name="stringToDecrypt">String to decrypt, must have been encrypted using EncryptionHelper.Encrypt with same cryptoKey and initVector.</param>
		/// <param name="cryptoKey">String to use as CryptoKey</param>
		/// <param name="initVector">byte[8] to use as initialization vector</param>
		/// <returns>String to use as CryptoKey</returns>
		/// <exception cref="IndexOutOfRangeException">IndexOutOfRangeException thrown if initVector is not 8 elements.</exception>
		public static string Decrypt(string stringToDecrypt, string cryptoKey, byte[] initVector)
		{
			return doDecrypt (stringToDecrypt, cryptoKey, initVector);
		}

    #endregion Public Static Methods

    #region Private Static Methods

		private static string doEncrypt(string stringToEncrypt, string cryptoKey, byte[] initVector)
		{
			if (initVector.Length != 8) {
				throw new IndexOutOfRangeException ("Initialisation vector initVector must be 8 values between 0 and 255");
			}
			if (String.IsNullOrEmpty (stringToEncrypt)) {
				return string.Empty;
			}
			string result = string.Empty;
			byte[] buffer = Encoding.ASCII.GetBytes (stringToEncrypt);
			using (TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider()) {
				using (MD5CryptoServiceProvider mD5 = new MD5CryptoServiceProvider()) {
					des.Key = mD5.ComputeHash (ASCIIEncoding.ASCII.GetBytes (cryptoKey));
					des.IV = initVector;
					result = Convert.ToBase64String (des.CreateEncryptor ().TransformFinalBlock (buffer, 0, buffer.Length));
				}
			}
			return result;
		}

		private static string doDecrypt(string stringToDecrypt, string cryptoKey, byte[] initVector)
		{
			if (initVector.Length != 8) {
				throw new IndexOutOfRangeException ("Initialisation vector initVector must be 8 values between 0 and 255");
			}
			if (String.IsNullOrEmpty (stringToDecrypt)) {
				return string.Empty;
			}
			string result = string.Empty;
			byte[] buffer = Convert.FromBase64String (stringToDecrypt);
			using (TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider()) {
				using (MD5CryptoServiceProvider mD5 = new MD5CryptoServiceProvider()) {
					des.Key = mD5.ComputeHash (ASCIIEncoding.ASCII.GetBytes (cryptoKey));
					des.IV = initVector;
					result = Encoding.ASCII.GetString (des.CreateDecryptor ().TransformFinalBlock (buffer, 0, buffer.Length));
				}
			}
			return result;
		}

    #endregion Private Static Methods

    #endregion Static Methods

    #region Constructors

		// Default constructor, scoped as private so this class cannot be instantiated.
		private Encryptor ()
		{
		}

    #endregion Constructors

	}


}
