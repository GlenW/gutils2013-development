/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GUtils2013
{

  /// <summary>
  /// <para>MandatoryPropertyAttribute</para>
  /// <para>To decorate a property that is to be va;idated by IDataSelfPopulater.ValidateMandatoryProperties()</para>
  /// </summary>
  [AttributeUsage(AttributeTargets.Property)]
  public sealed class MandatoryProperty : PropertyAttribute
  {

    #region Constructors

    /// <summary>
    /// Default
    /// </summary>
    public MandatoryProperty()
      : base()
    {
    }

    /// <summary>
    /// Sets Message = message
    /// </summary>
    /// <param name="message">
    /// A <see cref="System.String"/>
    /// </param>
    public MandatoryProperty(string message)
      : base(message)
    { }

    #endregion Constructors

  }
}
