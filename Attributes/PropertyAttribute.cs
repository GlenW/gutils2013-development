﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;

namespace GUtils2013
{
  /// <summary>
  /// Abstract base class for all PropertyAttribute types
  /// </summary>
  [AttributeUsage(AttributeTargets.Property)]
  public abstract class PropertyAttribute : Attribute
  {

    #region Properties
    /// <value>
    /// Message
    /// </value>
    public string Message { get; set; }

    #endregion Properties

    #region Constructors

    /// <summary>
    /// Default
    /// </summary>
    public PropertyAttribute()
      : base()
    { }

    /// <summary>
    /// Constructor sets Message = message
    /// </summary>
    /// <param name="message"></param>
    public PropertyAttribute(string message)
      : base()
    {
      this.Message = message;
    }

    #endregion

  }
}
