﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GUtils2013
{

  /// <summary>
  /// Allows binary (de)serialization to and from files
  /// </summary>
  public sealed class Serializer
  {


    #region Methods

    /// <summary>
    /// Binary serializes obj to file
    /// </summary>
    /// <param name="obj">object to seialize</param>
    /// <param name="file">file to serialize to</param>
    public static void BinarySerializeToFile(object obj, string file)
    {
      BinaryFormatter formatter = new BinaryFormatter();
      using (Stream stream = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None))
      {
        formatter.Serialize(stream, obj);
      }
    }

    /// <summary>
    /// Returns the contents of file binary deserialized as object 
    /// </summary>
    /// <param name="file">file to read from</param>
    /// <returns>Object</returns>
    public static object BinaryDeSerializeFromFile(string file)
    {
      BinaryFormatter formatter = new BinaryFormatter();
      using (Stream stream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
      {
        object obj = formatter.Deserialize(stream);
        return obj;
      }
    }

    /// <summary>
    /// Returns obj serialized to byte[]
    /// </summary>
    /// <param name="obj">object to serialize</param>
    /// <returns>byte[] of result of serialization of obj</returns>
    public static byte[] BinarySerialize(object obj)
    {
      BinaryFormatter formatter = new BinaryFormatter();
      using (MemoryStream stream = new MemoryStream())
      {
        formatter.Serialize(stream, obj);
        return stream.ToArray();
      }
    }

    /// <summary>
    /// Returns byte[] deserialized to object
    /// </summary>
    /// <param name="image">byte[] to deserialize</param>
    /// <returns>object of result of desrialization of image</returns>
    public static object BinaryDeSerialize(byte[] image)
    {
      BinaryFormatter formatter = new BinaryFormatter();
      using (Stream stream = new MemoryStream(image))
      {
        return formatter.Deserialize(stream);
      }
    }

    #endregion Methods

    // Default constructor, scoped as private so this class cannot be instantiated.
    private Serializer()
    { }

  }
}
