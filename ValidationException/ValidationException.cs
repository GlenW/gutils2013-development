/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Collections;
using System.Text;

namespace GUtils2013
{

  /// <summary>
  /// Exception object to be thrown during validation methods
  /// </summary>
  [Serializable()]
  public sealed class ValidationException : Exception
  {

    #region Constants

    /// <summary>
    /// Value used as key for constructor taking message parameter
    /// </summary>
    public const string INITIAL_EXCEPTION_KEY = "InitialExceptionKey";

    #endregion Constants

    #region Properties

    /// <summary>
    /// Returns each Exception's message seperated by Environment.NewLine
    /// </summary>
    public string FullValidationMessage
    {
      get
      {
        StringBuilder retVal = new StringBuilder();
        foreach (DictionaryEntry e in this.Data)
        {
          retVal.Append(e.Value.ToString() + Environment.NewLine);
        }
        return retVal.ToString();
      }
    }

    /// <summary>
    /// Returns whether there are any Exceptions in this
    /// </summary>
    public bool HasExceptions
    {
      get
      {
        return (this.Data.Count > 0);
      }
    }

    /// <summary>
    /// Returns the number of Exceptions in this
    /// </summary>
    public int ExceptionCount
    {
      get
      {
        return this.Data.Count;
      }
    }

    #endregion Properties

    #region Methods

    /// <summary>
    /// Add an exception to this
    /// </summary>
    /// <param name="key">Key of the Exception to be added.<see cref="System.Object"/>
    /// </param>
    /// <param name="message">Message for the Exception to be added.<see cref="System.Object"/>
    /// </param>
    public void AddException(object key, string message)
    {
      this.Data.Add(key, message);
    }

    /// <summary>
    /// Returns whether this has an exception with key = key
    /// </summary>
    /// <param name="key">Key of the Exception to look for.<see cref="System.Object"/>
    /// </param>
    /// <returns><see cref="System.Boolean"/>
    /// </returns>
    public bool HasException(object key)
    {
      return (this.Data.Contains(key));
    }

    /// <summary>
    /// Returns the Message assosciated with key = key
    /// </summary>
    /// <param name="key">Key of the Exception to look for.<see cref="System.Object"/>
    /// </param>
    /// <returns>Message for the Exception with key = key<see cref="System.String"/>
    /// </returns>
    /// <exception cref="IndexOutOfRangeException">IndexOutOfRangeException</exception>
    public string ExceptionMessage(object key)
    {
      if (this.Data.Contains(key))
      {
        return this.Data[key].ToString();
      }
      else
      {
        throw new IndexOutOfRangeException("No Exception with Key = key exists in ValidationException");
      }
    }

    /// <summary>
    /// Returns full Exception at posttion index in Data
    /// </summary>
    /// <param name="index">Index of the Exception to return</param>
    /// <returns></returns>
    /// <exception cref="IndexOutOfRangeException">IndexOutOfRangeException</exception>
    public DictionaryEntry Exception(int index)
    {
      if (this.Data.Count > index && index >= 0)
      {
        return (DictionaryEntry)this.Data[index];
      }
      else
      {
        throw new IndexOutOfRangeException("Index must > 0 and less than total number of items in Data");
      }
    }

    /// <summary>
    /// Removes all Exceptions from this.Data
    /// </summary>
    public void ClearExceptions()
    {
      if (null != this.Data && this.Data.Count > 0)
      {
        this.Data.Clear();
      }
    }

    /// <summary>
    /// Returns FullValidationMessage
    /// </summary>
    public override string Message { get { return this.FullValidationMessage; } }

    #endregion Methods

    #region Constructors

    /// <summary>
    /// Default
    /// </summary>
    public ValidationException()
      : base()
    {
    }

    /// <summary>
    /// Adds exception with key = INITIAL_EXCEPTION_KEY and message = message
    /// </summary>
    /// <param name="message">Message for the Exception to add.<see cref="System.String"/>
    /// </param>
    /// <remarks>This constructor uses INITIAL_EXCEPTION_KEY as key to Exception, therefore this key cannot be used again.</remarks>
    public ValidationException(string message)
      : base()
    {
      this.Data.Add(INITIAL_EXCEPTION_KEY, message);
    }

    /// <summary>
    /// Adds exception with key = key and message = message
    /// </summary>
    /// <param name="key">Key of the Exception to add.<see cref="System.Object"/>
    /// </param>
    /// <param name="message">Message of the Exception to add.<see cref="System.String"/>
    /// </param>
    public ValidationException(object key, string message)
      : base()
    {
      this.Data.Add(key, message);
    }

    #endregion Constructors

  }
}
