﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GUtils2013.DataSelfPopulater
{
  /// <summary>
  /// Implement this interface to simulate inheriting from DataSelfPopulater
  /// </summary>
  public interface IDataSelfPopulater
  {
  }
}
