﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Data;
using System.Reflection;

using System.Collections.Generic;
using GUtils2013.DataAccess;

namespace GUtils2013.DataSelfPopulater
{

  /// <summary>
  /// DataSelfPopulater
  /// </summary>
  [Serializable()]
  internal static class DataSelfPopulater
  {

    #region internal Methods

    /// <summary>
    /// Checks if all properties decorated with a MandatoryPropertyAttribute are set.
    /// Throws ValidationException if any are not set.
    /// </summary>
    /// <returns>true if all mandatory properties are set.</returns>
    /// <exception cref="GUtils2013.ValidationException">ValidationException</exception>
    internal static bool ValidateMandatoryProperties(IDataSelfPopulater obj)
    {
      return checkMandatoryProperties(obj);
    }

    /// <summary>
    /// <para>Populates this properties that are decorated with a InitialPopulatePropertyAttribute from reader.</para>
    /// <para>The ordinal of the field to use is attained from Property.Name, and the data type from the Property.PropertyType.</para>
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="obj">IDataSelfPopulator this method is called on</param>
    /// <exception cref="IndexOutOfRangeException">IndexOutOfRangeException thrown if no ordinal obtainable from a Property.Name</exception>
    internal static void PopulateFromDataReader(IDataReader reader, IDataSelfPopulater obj)
    {
      getFromDataReader(reader, obj);
    }

    /// <summary>
    /// <para>Saves properties decorated with SaveProperty attribute to sproc</para>
    /// <para>If a property is decorated with SaveReturnIDProperty its value will be set as the new/existing id returned by sproc</para> 
    /// <para>ValidateMandatoryProperties is called before saving, so ValidationException thrown if this fails.</para>
    /// </summary>
    /// <param name="data">IDataAccess object to use to perform save</param>
    /// <param name="sproc">Name of stored procedure to use to perform the save</param>
    /// <param name="obj">IDataSelfPopulator this method is called on</param>
    /// <exception cref="GUtils2013.ValidationException">ValidationException </exception>
    internal static void Save(IDataAccess data, string sproc, IDataSelfPopulater obj)
    {
      if (checkMandatoryProperties(obj))
      {
        saveToDatabase(data, sproc, obj);
      }
    }

    #endregion internal Methods

    #region Private Methods

    private static bool checkMandatoryProperties(IDataSelfPopulater obj)
    {
      ValidationException eX = new ValidationException();
      bool valueThere = true;
      foreach (PropertyInfo info in obj.GetType().GetProperties())
      {
        if (info.GetCustomAttributes(typeof(MandatoryProperty), true).Length == 1)
        {
          // Reference types
          if (null == info.GetValue(obj, null))
          {
            valueThere = false;
          }
          // Int16
          else if (info.PropertyType == typeof(Int16) && (Int16)info.GetValue(obj, null) == 0)
          {
            valueThere = false;
          }
          // int
          else if (info.PropertyType == typeof(int) && (int)info.GetValue(obj, null) == 0)
          {
            valueThere = false;
          }
          // Int32
          else if (info.PropertyType == typeof(Int32) && (Int32)info.GetValue(obj, null) == 0)
          {
            valueThere = false;
          }
          // string
          else if (info.PropertyType == typeof(string) && String.IsNullOrEmpty(info.GetValue(obj, null).ToString()))
          {
            valueThere = false;
          }
          // DateTime
          if (info.PropertyType == typeof(DateTime) && (DateTime)info.GetValue(obj, null) == DateTime.MinValue)
          {
            valueThere = false;
          }
          if (!valueThere)
          {
            eX.AddException(info.Name, info.Name + " is mandatory!");
          }
          valueThere = true;
        }
      }
      if (eX.HasExceptions)
      {
        throw eX;
      }
      return true;
    }

    private static void getFromDataReader(IDataReader reader, IDataSelfPopulater obj)
    {
      foreach (PropertyInfo info in obj.GetType().GetProperties())
      {
        if (info.GetCustomAttributes(typeof(InitialPopulateProperty), true).Length > 0)
        {
          getPropertyValueFromDataReader(reader, info, obj);
        }
      }
    }

    private static void getPropertyValueFromDataReader(IDataReader reader, PropertyInfo info, IDataSelfPopulater obj)
    {
      int ordinal = reader.GetOrdinal(info.Name);
      if (reader[ordinal] != DBNull.Value)
      {
        if (info.PropertyType == typeof(Int16) || info.PropertyType == typeof(Int16?))
        {
          info.SetValue(obj, reader.GetInt16(ordinal), null);
        }
        else if (info.PropertyType == typeof(Int32) || info.PropertyType == typeof(Int32?))
        {
          info.SetValue(obj, reader.GetInt32(ordinal), null);
        }
        else if (info.PropertyType == typeof(Int64) || info.PropertyType == typeof(Int64?))
        {
          info.SetValue(obj, reader.GetInt64(ordinal), null);
        }
        else if (info.PropertyType == typeof(Decimal))
        {
          info.SetValue(obj, reader.GetDecimal(ordinal), null);
        }
        else if (info.PropertyType == typeof(float))
        {
          info.SetValue(obj, reader.GetFloat(ordinal), null);
        }
        else if (info.PropertyType == typeof(Double))
        {
          info.SetValue(obj, reader.GetDouble(ordinal), null);
        }
        else if (info.PropertyType == typeof(TimeSpan))
        {
          info.SetValue(obj, (TimeSpan)reader[ordinal], null);
        }
        else if (info.PropertyType == typeof(byte))
        {
          info.SetValue(obj, reader.GetByte(ordinal), null);
        }
        else if (info.PropertyType == typeof(String))
        {
          info.SetValue(obj, reader.GetString(ordinal), null);
        }
        else if (info.PropertyType == typeof(Boolean) || info.PropertyType == typeof(Boolean?))
        {
          info.SetValue(obj, reader.GetBoolean(ordinal), null);
        }
        else if (info.PropertyType == typeof(DateTime) || info.PropertyType == typeof(DateTime?))
        {
          info.SetValue(obj, reader.GetDateTime(ordinal), null);
        }
        else if (info.PropertyType.IsEnum)
        {
          info.SetValue(obj, reader.GetInt32(ordinal), null);
        }
      }
      else
      {
        // If info is nullable (but not nullable value types like int?)
        if (info.PropertyType.IsGenericType && info.PropertyType.GetGenericTypeDefinition() == typeof(Nullable))
        {
          // Set it as null
          info.SetValue(obj, null, null);
        }
      }
    }

    private static void saveToDatabase(IDataAccess data, string sproc, IDataSelfPopulater obj)
    {
      List<object> paramList;
      PropertyInfo idProperty;
      populateSaveParams(out paramList, out idProperty, obj);
      if(idProperty != null)
      {
        int id = data.ExecuteIntProcedure(sproc, paramList.ToArray());
        idProperty.SetValue(obj, id, null);
      }
      else
      {
        data.ExecuteProcedure(sproc, paramList.ToArray());
      }
    }

    private static void populateSaveParams(out List<object> paramList, out PropertyInfo idProperty, IDataSelfPopulater obj)
    {
      paramList = new List<object>();
      idProperty = null;
      foreach (PropertyInfo info in obj.GetType().GetProperties())
      {
        if (info.GetCustomAttributes(typeof(SaveProperty), true).Length > 0)
        {
          paramList.Add("@" + info.Name);
          paramList.Add(info.GetValue(obj, null));
        }
        if (info.GetCustomAttributes(typeof(SaveReturnIDProperty), true).Length > 0)
        {
          idProperty = info;
        }
      }
    }

    #endregion Private Methods

    #region Constructors

    #endregion Constructors

  }
}

