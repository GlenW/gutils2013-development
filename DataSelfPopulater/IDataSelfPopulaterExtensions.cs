﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using GUtils2013.DataAccess;

namespace GUtils2013.DataSelfPopulater
{

  /// <summary>
  /// Extension methods to call DataSelfPopulater methods via IDataSelfPopulater
  /// </summary>
  public static class IDataSelfPopulaterExtensions
  {

    /// <summary>
    /// Checks if all properties decorated with a MandatoryPropertyAttribute are set.
    /// Throws ValidationException if any are not set.
    /// </summary>
    /// <returns>true if all mandatory properties are set.</returns>
    /// <exception cref="GUtils2013.ValidationException">ValidationException</exception>
    public static bool ValidateMandatoryProperties(this IDataSelfPopulater obj)
    {
      return DataSelfPopulater.ValidateMandatoryProperties(obj);
    }

    /// <summary>
    /// <para>Populates this properties that are decorated with a InitialPopulatePropertyAttribute from reader.</para>
    /// <para>The ordinal of the field to use is attained from Property.Name, and the data type from the Property.PropertyType.</para>
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="obj"></param>
    /// <exception cref="IndexOutOfRangeException">IndexOutOfRangeException thrown if no ordinal obtainable from a Property.Name</exception>
    public static void PopulateFromDataReader(this IDataSelfPopulater obj, IDataReader reader)
    {
      DataSelfPopulater.PopulateFromDataReader(reader, obj);
    }

    /// <summary>
    /// <para>Saves properties decorated with SaveProperty attribute to sproc</para>
    /// <para>If a property is decorated with SaveReturnIDProperty its value will be set as the new/existing id returned by sproc</para> 
    /// <para>ValidateMandatoryProperties is called before saving, so ValidationException thrown if this fails.</para>
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="data">DataAccess object to use to perform save</param>
    /// <param name="sproc">Name of stored procedure to use to perform the save</param>
    /// <exception cref="GUtils2013.ValidationException">ValidationException </exception>
    public static void Save(this IDataSelfPopulater obj, IDataAccess data, string sproc)
    {
      DataSelfPopulater.Save(data, sproc, obj);
    }

  }
}
