﻿/* *******************************************************************
* GUtils2013                                                         *
* Anyone is free to use this code as they see fit.                   *
* All I ask is that you keep this comment at the top of the code.    *
* Any suggestions or recommendation for future development email me. *
* Glen Wilkin                                                        *
* glenwilkin@gmail.com                                               *
**********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Reflection;
using System.Drawing;

namespace GUtils2013
{
  /// <summary>
  /// <para>General Extension Methods</para>
  /// <para>To make available in a project:</para>
  /// <para>Reference GUtils2013</para>
  /// <para>Add 'using GUtils2013;' to every class where needed</para>
  /// </summary>
  public static class ExtensionMethods
  {

    #region String

    /// <summary>
    /// Extends String.Replace to be case insensitive
    /// </summary>
    /// <param name="original">string original String to search</param>
    /// <param name="oldValue">string to find to be replaced</param>
    /// <param name="newValue">string to replace all instances of oldValue with</param>
    /// <returns>string original with oldValue replace with newValue</returns>
    public static string ReplaceNoCase(this string original, string oldValue, string newValue)
    {
      return Regex.Replace(original, oldValue, newValue, RegexOptions.IgnoreCase);
    }

    #endregion String

    #region List<T>

    /// <summary>
    /// <para>Sorts List by supplied property</para>
    /// <para>To use: MyList.Sort(item => item.Property)</para>
    /// </summary>
    public static void Sort<TSource, TValue>(this List<TSource> source, Func<TSource, TValue> selector)
    {
      var comparer = Comparer<TValue>.Default;
      source.Sort((x, y) => comparer.Compare(selector(x), selector(y)));
    }

    /// <summary>
    /// <para>Sorts as descending List by supplied property</para>
    /// <para>To use: MyList.SortDescending(item => item.Property)</para>
    /// </summary>
    public static void SortDescending<TSource, TValue>(this List<TSource> source, Func<TSource, TValue> selector)
    {
      var comparer = Comparer<TValue>.Default;
      source.Sort((x, y) => comparer.Compare(selector(y), selector(x)));
    }

    /// <summary>
    /// <para>Returns the List with duplicate items removed.</para>
    /// <para>First item that matches keySelector is kept.</para>
    /// </summary>
    /// <typeparam name="TSource">Generic List to remove duplicates from</typeparam>
    /// <typeparam name="TKey">Property to use for distinctness</typeparam>
    /// <param name="source">List</param>
    /// <param name="keySelector">Lambda function to describe property to use for ditstinctness(i => i.ID)</param>
    /// <returns>List</returns>
    public static List<TSource> DistinctBy<TSource, TKey>(this List<TSource> source,
        Func<TSource, TKey> keySelector)
    {
      return source.GroupBy(keySelector).Select(x => x.First()).ToList();
    }

    #endregion List<T>

    #region Windows Controls

    /// <summary>
    /// Sorts elements of list then binds dgv to list, if aleady sorted will sort descending
    /// </summary>
    /// <typeparam name="T">Type that dgv items are bound to</typeparam>
    /// <param name="dgv">DataGridView to sort</param>
    /// <param name="list">List T to be sorted</param>
    /// <param name="e">DataGridViewCellMouseEventArgs from dgv_ColumnHeaderMouseClick to identiy column</param>
    public static void SortDataGridView<T>(this DataGridView dgv, List<T> list, DataGridViewCellMouseEventArgs e)
    {
      string propertyName = dgv.Columns[e.ColumnIndex].DataPropertyName;
      if (dgv.Columns[e.ColumnIndex].Tag == null || !(bool)dgv.Columns[e.ColumnIndex].Tag)
      {
        list.Sort(i => i.GetType().GetProperty(propertyName).GetGetMethod().Invoke(i, null));
        dgv.Columns[e.ColumnIndex].Tag = true;
      }
      else
      {
        list.SortDescending(i => i.GetType().GetProperty(propertyName).GetGetMethod().Invoke(i, null));
        dgv.Columns[e.ColumnIndex].Tag = false;
      }
      dgv.DataSource = null;
      dgv.DataSource = list;
    }

    /// <summary>
    /// <para>Sets defaults for DataGridView</para>
    /// <para>AutoGenerateColumns = false</para>
    /// <para>AutoResizeColumns = DisplayedCells</para>
    /// <para>AlternatingRowsDefaultCellStyle.BackColor = WhiteSmoke</para>
    /// <para>BackgroundColor = Window</para>
    /// <para>DoubleMyBuffer(false)</para>
    /// </summary>
    /// <param name="dgv">DateGridView to set defaults for</param>
    public static void DefaultSetUp(this DataGridView dgv)
    {
      DefaultSetUp(dgv,
        false, 
        DataGridViewAutoSizeColumnsMode.DisplayedCells, 
        Color.WhiteSmoke, 
        Color.FromKnownColor(KnownColor.Window), 
        true, 
        false);
    }

    /// <summary>
    /// Sets properties for DataGridView
    /// </summary>
    /// <param name="dgv">DataGridView to set properties for</param>
    /// <param name="autoGenerateColumns">bool AutoGenerateColumns</param>
    /// <param name="autoResizeColumns">DataGridViewAutoSizeColumnsMode autoResizeColumns</param>
    /// <param name="alternatingRowsBackColour">Color alternatingRowsBackColour</param>
    /// <param name="backColour">Color backColour</param>
    /// <param name="doubleBuffer">bool doubleBuffer</param>
    /// <param name="virtualMode">bool virtualMode</param>
    public static void DefaultSetUp(this DataGridView dgv, 
      bool autoGenerateColumns,
      DataGridViewAutoSizeColumnsMode autoResizeColumns,
      Color alternatingRowsBackColour,
      Color backColour,
      bool doubleBuffer,
      bool virtualMode)
    {
      dgv.AutoGenerateColumns = autoGenerateColumns;
      dgv.AutoResizeColumns(autoResizeColumns);
      dgv.AlternatingRowsDefaultCellStyle.BackColor = alternatingRowsBackColour;
      dgv.BackgroundColor = backColour;
      if (doubleBuffer)
      {
        dgv.DoubleMyBuffer(virtualMode);
      }
      else
      {
        dgv.VirtualMode = virtualMode;
      }
    }

    /// <summary>
    /// Doubles the buffer of the DateGridView and sets its VirtualMode
    /// </summary>
    /// <param name="dgv">DataGridView to double the buffer of.</param>
    /// <param name="virtualMode">bool Value to set DataGridView's VirtualMode to</param>
    public static void DoubleMyBuffer(this DataGridView dgv, bool virtualMode)
    {
      typeof(DataGridView).InvokeMember("DoubleBuffered",
        BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
        null,
        dgv,
        new object[] { true });
      dgv.VirtualMode = virtualMode;
    }

    #endregion Windows Controls

    #region bool & bool?

    /// <summary>
    /// Returns Yes or No for bools
    /// </summary>
    /// <param name="source">bool</param>
    /// <returns>string "Yes" or "No"</returns>
    public static string AsYesNo(this bool source)
    {
      if (source)
      {
        return "Yes";
      }
      else
      {
        return "No";
      }
    }

    /// <summary>
    /// Returns Yes or No for bool?s
    /// </summary>
    /// <param name="source">bool?</param>
    /// <returns>string "Yes" or "No"</returns>
    public static string AsYesNo(this bool? source)
    {
      if (source == null)
      {
        return string.Empty;
      }
      if (source.Value)
      {
        return "Yes";
      }
      else
      {
        return "No";
      }
    }

    #endregion bool & bool?

    #region TryParse

    /// <summary>
    /// Parses string to Int32
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static int TryParseInt32(this string value)
    {
      return TryParse<int>(value, int.TryParse);
    }

    /// <summary>
    /// Parses string to Int16
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Int16 TryParseInt16(this string value)
    {
      return TryParse<Int16>(value, Int16.TryParse);
    }

    /// <summary>
    /// Parses string to Int64
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Int64 TryParseInt64(this string value)
    {
      return TryParse<Int64>(value, Int64.TryParse);
    }

    /// <summary>
    /// Parses string to Byte
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static byte TryParseByte(this string value)
    {
      return TryParse<byte>(value, byte.TryParse);
    }

    /// <summary>
    /// Parses string to Bool
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static bool TryParseBool(this string value)
    {
      return TryParse<bool>(value, bool.TryParse);
    }

    /// <summary>
    /// Parses string to Single
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Single TryParseSingle(this string value)
    {
      return TryParse<Single>(value, Single.TryParse);
    }

    /// <summary>
    /// Parses string to Double
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Double TryParseDoube(this string value)
    {
      return TryParse<Double>(value, Double.TryParse);
    }

    /// <summary>
    /// Parses string to Decimal
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Decimal TryParseDecimal(this string value)
    {
      return TryParse<Decimal>(value, Decimal.TryParse);
    }

    /// <summary>
    /// Parses string to DateTime
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static DateTime TryParseDateTime(this string value)
    {
      return TryParse<DateTime>(value, DateTime.TryParse);
    }

    #region Nullable Types

    /// <summary>
    /// Parses string to DateTime?
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static DateTime? TryParseNullableDateTime(this string value)
    {
      DateTime date;
      return DateTime.TryParse(value, out date) ? date : (DateTime?)null;
    }

    /// <summary>
    /// Parses string to bool?
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static bool? TryParseNullableBool(this string value)
    {
      bool b;
      return bool.TryParse(value, out b) ? b : (bool?)null;
    }

    /// <summary>
    /// Parses string to int?
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static int? TryParseNullableInt(this string value)
    {
      int i;
      return int.TryParse(value, out i) ? i : (int?)null;
    }

    #endregion Nullable Types

    private delegate bool ParseDelegate<T>(string s, out T result);
    private static T TryParse<T>(this string value, ParseDelegate<T> parse) where T : struct
    {
      T result;
      parse(value, out result);
      return result;
    }

    #endregion TryParse

  }
}
